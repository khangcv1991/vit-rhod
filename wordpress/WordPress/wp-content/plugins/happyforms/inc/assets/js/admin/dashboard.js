( function( $, settings ) {

	var happyForms = window.happyForms || {};
	window.happyForms = happyForms;

	happyForms.freeDashboard = {
		init: function() {
			$( document ).on( 'click', '#adminmenu #toplevel_page_happyforms li:last-child a', this.onUpgradeClick.bind(this) );
			$( document ).on( 'click', '#adminmenu #toplevel_page_happyforms li:eq(3) a', this.onResponsesClick.bind(this) );
			$( document ).on( 'click', '.happyforms-upgrade-modal .happyforms-continue-link', this.onContinueClick.bind(this) );
			$( document ).on( 'click', '.happyforms-upgrade-modal .happyforms-export-button', this.onExportButtonClick.bind(this) );
			$( document ).on( 'click', '.happyforms-upgrade-modal .happyforms-upgrade-modal__close', this.onCloseClick.bind(this) );
		},

		onUpgradeClick: function( e ) {
			e.preventDefault();

			var $link = $(e.target);

			window.open( $link.attr('href') );
		},

		onResponsesClick: function( e ) {
			e.preventDefault();

			tb_show( '', '#TB_inline?width=600&amp;inlineId=' + settings.upgrade_modal_id );
			$( '#TB_window' ).addClass( 'happyforms-admin-modal' ).addClass( settings.upgrade_modal_id );
			$( '#TB_ajaxContent' ).height( 'auto' );
		},

		closeThickbox: function() {
			tb_remove();
		},

		onContinueClick: function( e ) {
			e.preventDefault();

			this.closeThickbox();
		},

		onExportButtonClick: function( e ) {
			e.preventDefault();

			$( '.happyforms-upgrade-modal .happyforms-export-button' ).hide();
			$( '.happyforms-upgrade-modal form' ).addClass( 'shown' );
		},

		onCloseClick: function( e ) {
			e.preventDefault();

			this.closeThickbox();
		}
	};

	$( document ).ready( function() {
		happyForms.freeDashboard.init();
	} );

} )( jQuery, _happyFormsDashboardSettings );